import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import './ListOfNews.css';
import {SERVER_API_URL} from '../../config';
import Pagination from "../Pagination/Pagination";

const ListOfNews = () => {

    const [data, setData] = useState(null);
    const [currentPage, setCurrentPage] = useState(1);
    const [totalPosts, setTotalPosts] = useState(0);
    const size = 3;

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch(`${SERVER_API_URL}/api/newsposts/?page=${currentPage}&size=${size}`);

                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }

                const result = await response.json();
                setData(result.newsPosts);
                setTotalPosts(result.totalPosts);
            } catch (error) {
                console.error('Fetch error', error);
            }
        };

        fetchData();
    }, [currentPage]);

    const totalPages = Math.ceil(totalPosts / size);

    const handlePageChange = (page) => {
        setCurrentPage(page);
    }

    return (
            <div className="items-container">
                {data ? (
                    <>
                    <ul className="list-items">
                        {data.map(item => (
                            <li className="item" key={item.id}>
                                <Link to={`/news/${item.id}`}>
                                    <h4>{item.title}</h4>
                                </Link>
                                <p className="item-text">{item.text}</p>
                            </li>
                        ))}
                    </ul>
                        <Pagination currentPage={currentPage} totalPages={totalPages} onPageChange={handlePageChange} />
                    </>
                ) : (
                    <p>Loading...</p>
                )}

            </div>
    );
};

export default ListOfNews;

