import React, {useState} from 'react';
import { useNavigate } from 'react-router-dom';
import {SERVER_API_URL} from '../../config';
const FormCreate = ( { fetchData }) => {

    const [formData, setFormData] = useState({
        title: '',
        text: ''
    });

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData(prevState => ({
            ...prevState,
                [name]: value
        }));
    };

    const navigate = useNavigate();
    const handleBack = () => {
        navigate('/');
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await fetch(SERVER_API_URL + '/api/newsposts', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(formData)
            });
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            setFormData({
                title: '',
                text: '',
            });
            fetchData();
            navigate('/');
        } catch (error) {
            console.error('Error', error);
        }
    };

    return (
        <div className="form-container">
            <h2>Створення нової новини</h2>
            <form onSubmit={handleSubmit}>
                <label>
                    Title:
                    <input
                        type="text"
                        name="title"
                        value={formData.title}
                        onChange={handleChange}
                        required
                    />

                    Text:
                    <textarea
                        name="text"
                        value={formData.text}
                        onChange={handleChange}
                        required
                    />
                </label>
                <button type="submit" value="CREATE" onClick={handleSubmit}>Створити новину</button>
                <button onClick={handleBack}>Повернення до новин</button>
            </form>

        </div>
    );
};

export default FormCreate;