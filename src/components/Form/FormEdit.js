import React, { useEffect, useState} from 'react';
import { useNavigate } from 'react-router-dom';
import './Form.css';
import {SERVER_API_URL} from '../../config';

const FormEdit = ({id}) => {
    const [formData, setFormData] = useState({
        title: '',
        text: ''
    });

    const navigate = useNavigate();

    useEffect(() => {
        const fetchNewsData = async () => {
            try {
                const response = await fetch(SERVER_API_URL + `/api/newsposts/${id}`);
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                const result = await response.json();
                setFormData({
                    title: result.title,
                    text: result.text
                });
            } catch (error) {
                console.error('Fetch error', error);
            }
        };
        fetchNewsData();
    }, [id]);
    const handleSave = async () => {
        try {
            const response = await fetch(SERVER_API_URL + `/api/newsposts/${id}`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(formData)
            });
            if (!response.ok) {
                throw new Error('Network response was not ok')
            }
        } catch (error) {
            console.error('Error', error)
        }
    };

    const handleDelete = async () => {
        try {
            const response = await fetch(SERVER_API_URL + `/api/newsposts/${id}`, {
                method: 'DELETE'
            });
            if (!response.ok) {
                throw new Error('Network response was not ok')
            }
            setFormData({
                title: '',
                text: ''
            })
        } catch (error) {
            console.error('Error', error)
        }
    };

    const handleBack = () => {
        navigate('/');
    };

    return (
        <div className="form-container">
            <form>
                <label>
                    Title:
                    <input type="text"
                           name="title"
                           value={formData.title}
                           onChange={e => setFormData({...formData, title: e.target.value})}
                    />

                    Text:
                    <textarea
                    value={formData.text}
                    onChange={e => setFormData({...formData, text: e.target.value})}></textarea>
                </label>

                <button type="button" value="SAVE" onClick={handleSave}>Зберегти</button>
                <button type="button" value="DELETE" onClick={handleDelete}>Видалити новину</button>
            </form>
            <button onClick={handleBack}>Повернення до новин</button>

        </div>
    );
};

export default FormEdit;