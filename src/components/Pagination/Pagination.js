import React from 'react';
import './Pagination.css';

const Pagination = ({ currentPage, totalPages, onPageChange }) => {
   const isFirstPage = currentPage === 1;
   const isLastPage = currentPage === totalPages;

    const handlePrevClick = () => {
        if (!isFirstPage) {
            onPageChange(currentPage - 1);
        }
    };

    const handleNextClick = () => {
        if (!isLastPage) {
            onPageChange(currentPage + 1);
        }
    };

        return (
            <div className="pagination">
                {!isFirstPage && (
                    <button onClick={handlePrevClick} className="pagination-btn">
                        Попередня
                    </button>

                )}
                <span className="pagination-info">Сторінка {currentPage} з {totalPages}</span>

                {!isLastPage && (
                    <button onClick={handleNextClick} className="pagination-btn">
                        Наступна
                    </button>
                )}
            </div>
        );
};

export default Pagination;