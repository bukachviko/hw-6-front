import React from 'react';
import ListOfNews from "../components/ListOfNews/ListOfNews";
import { Link } from 'react-router-dom';



const Home = () => {

    return (
        <div className="main-container">
            <ListOfNews/>
            <Link to='/new'>
            <button>Додати новину</button>
            </Link>

        </div>
    );
};

export default Home;


