import React, {useEffect, useState} from 'react';
import {useParams} from 'react-router-dom';
import {Link} from 'react-router-dom';
import {SERVER_API_URL} from '../config';

const DetailsNews = () => {
    const {id} = useParams();
    const [news, setNews] = useState(null);

    useEffect(() => {
        const fetchNews = async () => {
            try {
                const response = await fetch(SERVER_API_URL + `/api/newsposts/${id}`);
                if (!response.ok) {
                    throw new Error('Network response was not ok')
                }
                const result = await response.json();
                if (!result) {
                    throw new Error('Empty response')
                }
                setNews(result);
            } catch (error) {
                console.error('Fetch error', error)
            }
        };
        fetchNews();
    }, [id]);

    return (
        <div className="item">
            {news ? (
                <div>
                    <h4>{news.title}</h4>
                    <p>{news.text}</p>
                </div>
            ) : (
                <p>Loading...</p>
            )}

            <Link to={`/edit/${id}`}>
                <button onClick={() => {
                }}>
                    Редагувати
                </button>
            </Link>
        </div>
    );
};

export default DetailsNews;