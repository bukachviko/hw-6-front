import React from 'react';
import { useParams } from 'react-router-dom';
import FormEdit from "../components/Form/FormEdit";

const EditNews = () => {
    const { id } = useParams();
    return (
        <div className="edit-news-page">
            <h2>Редагування новини</h2>
            <FormEdit id={id} />
        </div>
    );
};

export default EditNews;