import './App.css';
import {Routes, Route } from "react-router-dom";
import DetailsNews from "./pages/DetailsNews";
import CreateNews from "./pages/CreateNews";
import Home from "./pages/Home";
import EditNews from "./pages/EditNews";

function App() {

  return (
      <>
          <Routes>
              <Route index path="/" element={<Home/>}/>
              <Route path="news/:id" element={<DetailsNews/>} />
              <Route path="new" element={<CreateNews/>} />
              <Route path="/edit/:id" element={<EditNews/>} />
          </Routes>
      </>
  )
}

export default App;
